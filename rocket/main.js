import rocketName, {COUNT_DOWN_DURATION, launch } from './rocket.js';
//export string 'Saturn V' exported as the default member from rocket.js
//COUNT_DOWN_DURATION and launch() from rocket.js exported as named members
export function main () {
 console.log('This is a "%s" rocket', rocketName);
 console.log('It will launch in "%d" seconds.', COUNT_DOWN_DURATION);
 launch();
}
